---
title: Governance
...

Snowdrift.coop's team structure and governance is loosely adapted from various foundational concepts like [Sociocracy](https://en.wikipedia.org/wiki/Sociocracy) and [Holacracy](http://www.holacracy.org/) with less strict separation of the idea of "roles" versus people.

Our full governance docs live in the
[governance repository](https://gitlab.com/snowdrift/governance).

The following summarizes our basic approach:

## Roles

Ongoing *Domains* (i.e. areas of responsibility) for the project are separated into different roles, each with a set of accountabilities. The team member(s) holding a role can set the policies and workflow within the scope of the role.

Role holders may delegate any work to others (such as outside volunteers) but
will still retain accountability for the work in the end.

Beyond informal delegation, a role holder can split their accountabilities into a circle with multiple roles which other team members may hold.

## Tensions between roles

A role holder must get permission when taking any action that impacts the domain of a
different role.

As needed, we have governance meetings to discuss how to adjust roles, accountabilities, and so on in order to resolve tensions.
