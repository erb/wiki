---
title: About Snowdrift.coop
categories: communications
toc: false
...

Snowdrift.coop funds public goods through our innovative crowdmatching system. Get the basic gist from our [**60-second intro video**](https://archive.org/details/snowdrift-dot-coop-intro) (and our [old illustrated intro](/about/intro) offers a quick overview that covers a bit more detail).

Although you can read various pages here independently and in any order, the pseudo-book form below provides a logical reading experience to understand the background, values, essential funding mechanism, and overall structure that define Snowdrift.coop.

*Note: We strive to keep these pages up to date but things may slip by. You can help us fix inconsistencies by [suggesting edits](/Help#contributing) or otherwise contacting us. Thank you!*

## **I. Funding of public goods**

1. [Foreword: Systemic problems require systemic solutions](/about/premise)
#. [The Snowdrift Dilemma](/about/snowdrift-dilemma)
#. [The Economics of Public Goods](/about/economics)
#. [Community Projects and Economic Psychology](/about/psychology)
#. [Markets and Prices](/about/markets-and-prices)
#. [Cooperation and Competition](/about/compete)
#. [Existing Funding Options for Non-Scarce Projects](/about/existing-mechanisms)
    * [Issues with campaign crowdfunding](/about/threshold-systems)

## **II. Snowdrift.coop the organization**

8. [Mission](/about/mission)
#. [A Platform Cooperative](/about/co-op)
#. [Effective Democracy](/about/democracy)
#. [Why Only Free/Libre/Open Projects?](/about/why-flo)
#. [What Does *Free/Libre/Open* Mean?](/about/free-libre-open)
#. [FLO Licensing Discussion](/about/licenses)
#. [Fundraising for Charities and Other Purposes](/about/other-fundraising)

## **III. Our crowdmatching mechanism**

15. [The Snowdrift.coop Funding Mechanism](/about/mechanism)
#. [Limits to Pledge Amounts](/about/limits)
#. [The Meaning of a Crowdmatching Pledge](/about/pledging)
#. [Snowdrift.coop Advantages](/about/advantages)

## **IV. Building the community**

19. [Types of Projects](/communications/project-types)
    a. [Music and arts](/communications/intro/artists)
    a. [Education](/communications/intro/educators)
    a. [Software programs](/communications/intro/developers)
    a. [Journalism](/communications/intro/journalists)
    a. [Research](/communications/intro/researchers)
#. [Project Requirements](/project-reqs)
#. [Honor System](/community/honor)
#. [Snowdrift.coop Code of Conduct](/community/conduct)
#. [Honor Code for General Users](/community/honor/users)
#. [Honor Code for Projects](/project-reqs/honor-projects)
#. [How to Help Snowdrift.coop Succeed](/community/how-to-help)

## **Appendices** and further readings

A.  [FAQ (Frequently Asked Questions)](/about/faq)
A.  [Glossary of Terms Used at Snowdrift.coop](/about/terminology)
A.  [Fair Use](/about/fair-use)
A.  [Past Presentations about Snowdrift.coop](/about/presentations)
A.  [Team](/community/team)
A.  [Advisors](/community/advisors)
A.  [Partners](/community/partners)
A.  [Snowdrift.coop History](/about/history)
A.  See more references in our wiki pages on [market research](/market-research)
