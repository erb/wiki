# Governance Meeting — March 30, 2016

Attendees: smichel17, fr33domlover, chreekat, iko, wolftune, salt, msiep

---

## A few Holacracy concepts clarified (smichel17)
    
**Lead Link**  
1. ensures that the Circle's work gets done, particularly managing Circle boundaries (e.g. people outside the Circle putting demands on people within) ensures the function of the Circle (e.g. this work needs to be done by x role)  
2. strives to spend as little time as possible in that role. Since things not assigned to a role are picked up by the Lead Link, the aim is to go get items assigned to a role as soon as possible  

**Policies**  
* A policy is attached to a domain specifically and only affects that domain  
* If it's a domain at the Circle level, then it has to be done at the governance level  

**Reminder for governance meetings:**  
1. Bring proposals to governance meeting, not just tensions  
2. Mention a specific example of where the tension is sensed and how the proposal will help  

**Tension Template**  

```
### Tension:

**Proposal:**
**Proposer:** 
**Clarifications:**
**Objections:**
**Outcome:**
```

List of tensions: https://tree.taiga.io/project/snowdrift/issues  
Note: we are currently using Taiga Issues for governance tensions, but this is temporary. Once we have githost instance, the governance docs will be moved over. There will be an Issues section in the governance docs repo that will be used instead.

---

## Minutes

### Tension: Issues as tensions 

**Proposal:** we don't have issues anywhere at all  
**Proposer:** chreekat  
**Clarifications:** this applies to governance tensions. It's impacting my work load as scrum master, it's the secretary's job to gather them up, and I'd rather rely the secretary to collect and bring to people's attention at the governance meeting. Once it's formalised, I would have to add to it according to procedure somewhere, and I feel like it's solving a problem that doesn't exist.   
**Objections:**   
* smichel17: I don't feel that there will be a good forum to discuss issues with this removed, however in an agile approach we can bring issues up for discussion as needed  
**Outcome:** governance tensions will no longer be posted in Taiga Issues


### Tension: User research vs. market research 

**Proposal:** there should be two different roles, one for user feedback and another for funding platforms  
**Proposer:** wolftune  
**Clarifications:** I feel like market research is a confusing term, suggestions welcome  
**Objections:**  
* smichel17: currently not valid governance output because we can't create role without specific wording
**Outcome:**  
* Research => user research - People interacting with snowdrift.coop site and ecosystem  
*  Market Reasearch - Boarder ecosystem outside snowdrift.coop, history of public goods and related concerns  


### Tension: Team diversity 

**Proposal:** we should have a stated goal to promote diversity  
**Proposer:** wolftune  
**Clarifications:** a policy attached to all functions within the Circle, or is it a Community thing? We add a policy to the domain of bringing on new partners and this same policy should also be in the Community outreach role.  
* smichel17: on second thought, Lead Link has authority to determine this  
* wolftune: if people didn't have that policy before, how to apply it?  
* smichel17: however, it needs to be published in governance docs  
**Objections:** -      
**Outcome:** to be further discussed later  


### Tension: Remove PM role 

**Proposal:** PM doesn't have anything left to do and could be removed outright. Interpreting the Constitution is the Secretary's role  
**Proposer:** smichel17  
**Questions:**  
* wolftune: does this affect external-facing materials, e.g. business cards with PM title printed?  
* smichel17: no, this only affects governance  
* chreekat: there are points in the governance doc for that role that are not captured in the points listed  
**Clarifications:** we don't have milestones, and issues are the responsibility of disciplines to bring up. The governance doc doesn't reflect reality  
**Objections:**  
* chreekat: PM will have varying levels of work depending on whether things are moving smoothly. There still needs to be someone to track progress and say we are using x tools, e.g. Taiga, and for that we need a scrum master. As a developer, it's good to have someone checking on progress and speed, e.g. for milestones.  
* wolftune: there's a case for keeping the role around for the things it was designed for, as we may need it again  
**Amendment:** clarify PM role with wording re-written  
**Outcome:** PM role revised  

```  
Project Manager:  
    - Keeping up with overall progress of the project and providing insight into that progress upon request  
    - Advising on tools  
```

### Tension: Discuss ML gets too much traffic 

**Proposal:** introduce Discourse for org-wide communication/making announcements  
**Proposer:** smichel17  
**Note:** withdrawn due to clarified role of PM (no longer a governance issue)  


### Tension: All roles accountable to respond to requests for info from communications 

**Proposal:** all roles accountable to respond to requests for info from communications  
**Proposer:**  wolftune  
**Clarifications and Reactions:**   
* chreekat: is this going to policy for a team member?  
* iko: sounds odd to me, teams regularly make requests to other teams for info or other  resources with the expectation of response at some point. There's already a process  (via Taiga task blocks and triaging at tactical meetings) for calling on people to  provide resources needed for other teams' tasks. If this is codified, it should be done for all team  data flows   
* Salt: agreed with what was said  
* wolfune: maybe there's no need to make this explicit in governance docs  
**Outcome:**: proposal withdrawn  


### Tension: What is security? 

**Proposal:**
```
Security Role:
    Domain:
        Team Security - ensures backups are secure / internal authorization (to access domain servers, etc)
        User Security - ensures user data is secure
 Accountability
    - Creating guidelines on security issues
    - Creating security documentation
    - Idenfity where we're most vulnerable, high priority issues
    - Avoiding money fraud
    - Managing patrons' financial info
    - Managing patrons' identity data
    - Mitigating unauthorized access to servers
    - Securing backups
    - Defining access control schemes
    - Mitigating unauthorized account usage
        - on Snowdrift.coop, taiga.io, and any other systems
```
**Proposer:** tension originally proposed by chreekat, proposal presented by Salt  
**Clarifications:**  
* wolftune: does this role also keep track of authorisation/clearance?  
* Salt: this role creates the guidelines of how it's managed, not necessarily the same person who does it  
**Objections:** -  
**Outcome:** new role created


### Tension: rename the GM role to Lead Link

**Proposal:** rename the GM role to Lead Link  
**Proposer:**  smichel17  
**Clarifications:** no accountabilities changed, hiring is done by GM  
**Objections:** -  
**Outcome:** GM role to be renamed to Lead Link  

---

## Elections for facilitator, secretary and rep link

Process: see HolacracyOne's [Integrative Election Process](https://github.com/holacracyone/Holacracy-Constitution/blob/master/Holacracy-Constitution.md#336-integrative-election-process)


### Nominations for Secretary

Nominations:

    smichel17: smichel17
    chreekat: smichel17
    Salt: iko
    iko: smichel17
    wolftune: abstain

Reasons for nomination: 

* wolftune: smichel17 has been filling most of the secretary accountabilities and doing a great job
* Salt: iko takes good notes

**Outcome:** smichel17 is secretary for two months


### Nominations for Rep Link

Note: Lead Link cannot be the facilitator or rep link
Rep Link: represents a sub-Circle to the main Circle, representative to the Board

Nominations:

    smichel17: abstain (no strong preference)
    chreekat: abstain (no strong preference)
    Salt: Salt
    iko: Salt
    wolftune: Salt

Reasons for nomination: 
    
* wolftune: I think Salt is good at thinking about all the people involved
* iko: agreed with wolftune

**Outcome:** Salt is rep link or two months


### Nominations for Facilitator

Nominations:

    smichel17: Salt
    chreekat: abstain
    Salt: smichel17
    iko: smichel17
    wolftune: smichel17

Reasons for nomination:
    
* wolftune: it would be awesome to have same person as facilitator and secretary
* iko: smichel17 is most knowledgeable in the Holacracy process and is good at mediating discussions
* Salt: agreed with wolftune and smichel17 has experience with Holacracy
* smichel17: the real skill needed is sticking to the process, Salt has the personality and mediate tensions well

Change of vote:  
* Salt: Salt - reason: I feel like it's something I can step into  

**Outcome:** smichel17 is facilitator for two months

