# Meeting — July 18, 2016

Attendees: Salt, Robert, Bryan, Stephen, Michael (msiep), Aaron


## Check in

browsable, clickable prototypes via seafile

msiep: Had an idea & prototype (?) for the dashboard based on something mray did, will share soon.

wolftune: going to represent snowdrift at an upcoming crowdfunding conference, hopes it's not a waste of time. Legal issues on the todo list.


## Agenda

* salt - taiga, splitting projects
* mray - creation of wireframes
* bryan - roadmap for mvp
* (michael - afk?)
* aaron - help aaron stay on task :)
* bryan - auth subsite
* bryan - jitsi is t3h l4m3z0rz


## Taiga

- As of last discussion, decision was made to split off outreach section out of the main Taiga project
- But maybe other direction is better: split off MVP into its own project
- The Snowdrift project, right now, is a lot of things; open-ended. But MVP is close-ended. Scrum masters agree on this
- Next action: Schedule a meeting for pulling things, Wednesday morning

## Wireframing / prototype workflow

- mray has put something on the wiki, needs feedback on their completeness and usefulness, and also if it's best way to share them: <https://wiki.snowdrift.coop/page-specs/dashboard#main-elements>
- With corresponding wireframe: <https://snowdrift.sylphs.net/f/dd072ca786/>
use a different font / color to indicate "This is a proposed wording"
- Since mray isn't sure at this stage whether he needs to get the words right, or just use filler or boilerplate
- Problem is letting other people know what is or is not boilerplate, but without also impacting the UI/style too much
- Maybe use some key words like "Lorem ipsum", so we can have "useful" boilerplate but know it's still boilerplate
- But maybe we should just treat every wireframe as tentative until it's something is truly "done", so the base assumption is "This is a tentative version"
- If phrasing is good, it doesn't matter if it's also placeholder from early in the design...
- If we're commenting on a wireframe, should be free to comment on any of it
- Just have some notes that go with a mockup, like "This is just an early version, not a lot of thought has gone into the phrasing etc" or "this is fairly finished, just working on X"
- Is there a way we could standardize where the notes attached to a wireframe go?
- "... like Taiga?"
- So discussion starts on Taiga issue/US, which links to the wireframes/mockups. Ideally there's a social convention that any links to wireframes and mockups are accompanied with a short blurb describing the status of things
- Gitlab Pages as a solution?
- Bryan would hate to go to the back dark ages of using folder names for version control
- ..Maybe we just keep using the existing system for now (plus the policy of always describing the state of a mockup when you link it)


## Roadmap for MVP

- 2-hour brainstorming session with security/sys-ops for identifying attack vectors. We may not get solutions for all, but we should at least know what we'd have to do if that happened. Just in case. Or if something good happens, like a huge user spike.
- Bryan is a person who needs a plan. He'd like to time box: "For two weeks, we'll work on XYZ, then decide if it's good enough, or have a better idea of what we really need etc.
- wolftune's biggest tension: This roadmap is a major part of snowdrift's success. It should be more public.
- Next action: talk about this more on Wednesday

## Keeping Aaron (and others) on task

- Salt thinks having a roadmap is going to resolve this.
- Aaron has been avoiding discusing personal things. Has a big backlog. Is in prep for moving, taking the opportunity to try to establish new habits in the new context. Feeling: paired work is more focused & productive (stephen says +1). Could we do pair work more often?
- Salt: It's helpful to have a place to "log in" and see other people who are also working.
- chreekat: Having a specialized time be a good boundary?
- wolftune: Long-term, it's a good idea for people to be able to be personable, not purely work. But I also want people to encourage others to be productive.
- chreekat: 18:00-19:00 UTC is a great time for me to do group work
- smichel17: Two IRC channels? snowdrift (idle chat, stay logged in all the time, not logged) & snowdrift-dev (join to "log in" when you're working, logged, focused on work)
- chreekat: I think we should try just the one channel first (reasons were included, too much to type, they made sense)
- mray: could we hang out idly in jit.si?
- many: no, for various reasons
