# Meeting 9

*November 23rd 2013, 12PM Eastern, 10AM Mountain, 9AM Pacific*  
*phone chat on freeconferencecall.com (605) 475-4000 pin 564511#  
and live-recording of minutes on [etherpad](https://snowdrift.etherpad.mozilla.org/1)*

## Background readings / updates / tasks

### Background readings / updates

Please fully explore the post PiP site, submit any bug or feature tickets that arise from your exploration, and come prepared to discuss recruiting new coding volunteers and getting the site ready to function.

Please read the [fund drive](fund-drive) page over and come prepared to discuss what expenses we may wish to crowdfund and how we should schedule fundraising efforts.

### Assigned tasks
* Aaron: working on grant application, follow up on infographic, more research on funding
* James: check in on Standing Rules progress.
* Kim: contact Deb, EFF, NCB, get all the agenda items left hanging moved to the web
* Mike: check progress on WebRTC or any other possible teleconference solution
* David: PiP rollout

## Agenda
* Check in with assigned tasks from previous meeting, agenda review
* Technical update
    * Next steps post-PiP
        * Moderation and permissions especially high priority to fix
        * Site illustrations preparing to go live very soon
    * Volunteer and recruitment needs
        * Can begin more active recruitment of coding volunteers
        * Potential need to recruit vice Kim
* Funding discussion
   * Grant progress
        * Shuttleworth Fellowship
        * Ashoka Fellowship
        * Additional opportunities to pursue?
   * Crowdfunding campaign
       * What expenses are suitable for crowdfunding?
       * How long would it take us to prepare for a campaign?
       * What would be an ideal time for the campaign to take place? 
* Steering Committee / potential Board of Directors recruitment
    * Reach out to previous members to ask if they can recommit?
    * Ask current volunteers about interest?
        * [Peter St. Andre](https://snowdrift.coop/user/141) specifically expressed interest in working with Mike Linksvayer
        * Kim is switching to volunteer status and is willing to commit
    * Seek new partners?

## Next steps assigned

* Aaron: Knight prototype, messaging, illustrations, coding. Talk to James about status-quo background research on education
* David: Test database for certain, commit to permissions work, maybe moderation.
* Mike: WebRTC more. Coordinate with Kim on crowdfunding expense research.
* Kim: Research crowdfunding expenses, work with Aaron to determine what tasks might be contracted.
* James: Coordinate with Kim and Aaron on and possibly take over work on /status-quo-education. 

## Summary / minutes

*November 23rd 2013, 12PM Eastern, 10AM Mountain, 9AM Pacific
phone chat on freeconferencecall.com (605) 475-4000 pin 564511#
and live-recording of minutes on etherpad*

* Meeting convened at: 10:06, James absent.
* Check in with assigned tasks from previous meeting, agenda review
    * Aaron: Applied to both Shuttleworth and Ashoka, no word yet. Going ahead on own initiative with infographic, due to lack of commitment from artists. Still in progress. Research completed on other crowdfunding but more research needed on grants.
    * James: Absent.
    * Kim: Deb in the loop about progress. Reached out to EFF, no response. Our application is in with NCB, but they recommended we wait until we have a real board. Will process before that if we ask them too, though. Fundraising issues moved to the web, agreement reached on grants and crowdfunding.
    * Mike: Tried a couple WebRTC options yesterday, none were at the point of being usable but closer than before.
    * David: Also applied for Shuttleworth Fellowship. First major push for PiP done.
* Agenda approved
* Technical update
    * Most features have now been moved per-project (wiki, userlist, feed, etc).
    * Permissions changing dramatically, still not fully fleshed out.
    * Snowdrift admin should be able to create admin invite for other project, admin can then invite other people. Need to test this soon.
    * Since then, Kim and Aaron have merged some bugfixes/formatting changes, David's changes since have not been pushed.
        * Do we have a sense of when the site will be ready for more users? Not a clear timeline at this point.
    * Next steps post-PiP
        * Moderation and permissions especially high priority to fix.
        * Contribution process needs to be improved soon too.
            * Test database needed first-thing, so that changes can tested locally before merging. Probably highest priority of all.
            * Lower priority than moderation: videos on how to contribute through Github.
        * Site illustrations preparing to go live very soon
            * Aaron has been working on infographic.
            * Aaron and Kim have picked out Nina Paley cartoons for certain pages. [On MoPad](https://snowdrift.etherpad.mozilla.org/7).
            * Aaron also completed a stand-alone illustration of the additional share matching process.
        * Has anyone noticed any bugs that have not been reported?
            * Everything found has gone on tickets.
    * Volunteer and recruitment needs
        * Can begin more active recruitment of coding volunteers
            * General issue of outreach, experienced Haskellers, free software people, etc.
            * Anticipation of lots of promoting after fixing moderation etc. This could be moved up if needed.
            * Contact previous contributors with specific tickets to fix? Got to Haskell and Yesod IRC channels to recruit?
            * Expected return from general coders should be very low in the short term, better to concentrate on getting site ready.
            * For existing contributors, if you have a specific task, go for it but don't spend a lot of time trying to match people to tasks.
            * Is there a long-term trade-off to not encouraging newcomers now?
            * Return will probably be higher if people reach out to us rather than the reverse.
            * Many contributors will probably work on site content or recruiting projects rather than coding.
            * Consensus: not so worth it right now but after site fixes will be worth hand-holding newbies who come to us interested.
        * Potential need to recruit vice Kim?
            * Coding is primary hold-up.
            * Legal research, Bylaws, etc are done for now. High priority contract tasks are at stable point.
* Funding discussion
    * Grant progress 
        * Shuttleworth Fellowship
            * Both Aaron and David have applied, submission confirmed, no substantive reply yet.
        * Ashoka Fellowship
            * Aaron applied via self-nomination, submission confirmed, no reply yet.
            * David may want to apply as well, if he has the time.
            * Have a somewhat later lifecycle focus than Shuttleworth, so may want to reapply if we get turned down the first time.
        * Additional opportunities to pursue?
            * May want to delay applications until site work is done, so the demo is more active.
           * Research for now more effective use of time.
           * Knight Foundation Prototype Grant? Faster turnaround than the Fellowships, funded Spot.us failed journalism crowdfunding site, if we can show we address those problems we have a good chance.
               * Aaron will take a look today as they likely won't be turned off by a partially functional site.
           * Issues with grant type: most social orgs want 501(c)(3), entrepeneurship grants want funding stream/profits cut which we can't offer.
           * Should focus primarily on internal funding issues instead of grants for now.
    * Crowdfunding campaign
        * What expenses are suitable for crowdfunding?
            * $3000 retainer for Deb for legal work.
            * Living expenses for Aaron so he doesn't have to recruit more music students.
            * Alternate salary for David (would require a larger amount).
            * Is the second major purpose of the crowdfunding recruitment/generating interest?
            * Difficulty in that we will do these things regardless, if the campaign fails we will still get them done.
            * [Git Annex example](https://campaign.joeyh.name/blog/)
            * Add stretch goals as we go?
            * Find contract coder / illustrator / web designer? Probably not going to be able to find a US coder who would be willing to contract for what we would be able to pay, who is not already interested in and sympathetic with our campaign. Need to find individual people who could tell us what their price would be.
        * How long would it take us to prepare for a campaign? / What would be an ideal time for the campaign to take place?
            * Holiday season might be ideal but this one is coming up too soon and next year is longer than we want to wait.
            * Before the economy crashes again / the Fed stops quantitative easing, especially if we get some sort of loan.
        * External or self-hosted?
            * Self-hosting best way to go.
            * Coordination with another organization to take payments (like MediaGoblin with FSF), or after we get set up with Balance to take funds directly?
            * If we do threshold and self-host, we have to return funds and eat transaction costs after failure.
            * Aaron inclined to self-hosted weak threshold as way forward.
            * Software Freedom Conservancy is having payment problems, will not be able to help us.
        * Set rewards and donation levels when we have a number for what we want to raise.
* Steering Committee / potential Board of Directors recruitment
    * Mike wants to get Board set up ASAP, need legal and financial advice, not a strong presence for that yet, also need representatives from potential Snowdrift projects.
    * Ask current volunteers about interest?
        * Tobias may be most committed of those not formally connected with the project yet.
        * Jim Hazard for legal expertise? Not going to want to give formal advice but might at least have that perspective. May only want to offer feedback. Consider recruiting Jim for steering committee?
        * Peter St. Andre specifically expressed interest in working with Mike Linksvayer. Mike emailed him, too busy for more than comments at this point.
        * Kim is switching to volunteer status and is willing to commit
            * Still want Kim on the Board.
    * Seek new partners?
       * Make it clear that this is an option — this is mentioned on the recruitment page.
       * Mention desired qualities more clearly?
* Checkouts
