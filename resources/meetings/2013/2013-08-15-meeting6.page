# Meeting 6

*5:30PM Mountain Thursday August 15th 2013*
*phone chat on freeconferencecall.com (605) 475-4000 pin 564511#  
and live-recording of minutes on [etherpad](https://snowdrift.etherpad.mozilla.org/1)*

## Background readings / updates / tasks

* [Read this comment](wiki/comment/332) and bring any thoughts you have on permissions post-PiP
* Please review the new [volunteer application](https://snowdrift.coop/volunteer) and bring any suggestions (as well as suggestions for the in-progress project application).
* If possible, please take a look at the revised [project requirements](project-reqs) and [project honor code](honor-projects) and share any thoughts about whether items are appropriate and correctly prioritized.

## Agenda

* Attendance, check-ins: What do we want out of this meeting? (5 minutes)

* Assigned tasks from Meeting 5: (5 minutes)
    * Kim: investigate legal options, work with Greg and James?
    * Greg (if present): developer contacts?
    * James: Bylaws?
    * Mike: work with David on developer recruitment, contact Chris Sakkas?
        * *note from Aaron: Chris was an applicant under old committee form, but really belongs more with volunteer form, and I've been in touch a little*
        * *note from Mike: I've not spoken w/Chris; could use clarification about what -- find out what kind of volunteer work he wants to do? On developer recruitment, I just pinged David, and hope to meet or otherwise discuss w/him soon.*
    * Aaron: new mission statement? CLS/OSCON report pushed to next item.
    * David: technical report pushed to main item.

* Aaron's report from CLS and OSCON: (15 minutes, if shorter any extra time can be moved to next item)
    * Ideas that have now been incorporated into the site, esp. volunteer engagement
    * Connections made that may be valuable (especially for legal assistance).

* Primary issue: approaching opening up the site fully (30 minutes)
    * David's technical update.
    * [List of plans/requirements](wiki/comment/332) for new site milestone: defining user roles post-PiP.
    * Discussion about further technical steps needed to fully open site, assignment of tasks.
    * Non-technical progress so far and other steps required for site opening.
        * Setting up full volunteer intake process.
        * Policy documents / Privacy issues.
        * Engagement, introductions, market research.
    * Set ChickTech Non-profit Storming priority and assign steps.
    * Networking and marketing: are we ready now or should we continue to wait for site progress?

* Open forum on meeting timing, regularity, and coordination. (10 minutes)
    * Do we need more chatting between meetings?
    * Should we get more clarity about when is good and make it easier for more people to be involved?
    * Do we feel the timing is appropriate?
    * Should we allow a non-committee member into meetings as a recorder so Kim can concentrate just on facilitation?

* Review of assigned tasks, check-outs (5 minutes)

## Next steps assigned

 * Kim: get a monthly meeting time established and weekly IRC open house, and start recruiting for a recorder, work w/ James bylaws.
 * Aaron: scrub of dev site, connect w/ David on technical stuff, ToU run-through, contacts from conferences.
 * Mike: follow up on Aaron's ChickTech "problem to solve", connect w/David re tech recruiting, ToU run-through
 * David: PiP ready to go live, poke moderation discussion, ToU run-through.
 * James: work w/ Kim on bylaws, ToU run-through.
 * Greg: ToU run-through. confirm his time availability etc. (Greg should have insights for ChickTech too, he's done lots of similar events)

## Summary / minutes

* Present: Kim, James, Mike, David, Aaron
* Meeting started at 17:45 due to technical issues w/ conference line.
* Any check-ins? No.
* Any changes to agenda? No.
* Any changes to minutes? No.
* Reports on assigned tasks from Meeting 5: 
    * Kim has been working on getting legal stuff figured out (we have settled when we want to get Deb involved), setting up volunteer process.
    * Greg: with baby! No report. 
    * James: been working on developing board member training. 
    * Mike: exchanged emails w/ David on developer contacts, Aaron got in contact with Chris and he's more of a volunteer than a potential committee member. 
    * Aaron: revised mission substantially over the month. Agreement? Mike, Kim, David would like some more time to decide, James.
* Aaron's convention report:
    * Community Leadership Summit and Open Source Conference.
    * Met lots of interesting people, handed out lots of business cards.
    * Made a lot of changes in wiki based on ideas gained in talking to people.
    * Developing better system to invite volunteers and identify skills and interests of volunteers.
    * Need to do more follow-up with people who gave business cards.
    * Legal assistance offers: Democracy Lab, Mozilla Foundation, HP being a sponsor w/ free server on HP Cloud, Jim Hazard from Common Accord
    * Follow up on any other questions on email or IRC.
* Opening site fully:
    * David's update: several PiP items already written and pushed, running on dev, need other people to go through and check for other smaller things that need to change after big changes, gross changes have been made but permissions and roles changes have yet to be written.
        * Aaron intends to go through and start looking for that stuff.
        * Would this be better to give to someone else so that Aaron has more time to do networking?
        * What we need is to go through dev site and make sure that PiP changes are not breaking links, breaking site navigation or making wording obsolete.
        * Ticket for seeking out broken wiki links?
    * Other technical issues: get forms launched, get moderation systems set up--how do we want to create that framework?
        * Will work out framework for moderation on wiki, can probably farm out some parts to other programmers, or if someone else wants to take it on entirely David can show them what to do.
    * Non-technical issues: Bylaws, volunteer process, ToU/Privacy Policy
        * Volunteer process should be good to go live
        * ToU/Privacy Policy is most important because we need it for volunteers to test site even before test of pledge system.
        * Committee members will all review those documents before the next meeting, be ready to approve or change them, and make sure they go to the lawyer before we open for any financial transactions.
    * ChickTech priority? Will consense over email.
    * Will wait until PiP and ToU are done to start aggressively marketing.
* Meeting timing, regularity, coordination:
    * One per calendar month, spaced as evenly as possible.
    * Set an established time each month? Email Kim general availability.
    * Set a weekly open-house on IRC? Email Kim general availability.
    * Okay with getting someone else to do recording in meetings.
* Checkouts: Kim will try to improve flow and have more defined problems to solve for next meeting.
