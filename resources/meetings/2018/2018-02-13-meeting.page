# Meeting — February 13, 2018

Attendees: iko, MSiep, Salt, smichel17, wolftune, mray, chreekat

<!-- Agenda -->

## How to mark that I'm working on an issue in Design repo
- MSiep: I used the existing tag "Doing", but just having it assigned to me plus tag is enough? ("I'm working it, don't pay attention to it")
- chreekat: yeah, that's enough, "Doing" has some GitLab automatic connection to the [kanban view](https://git.snowdrift.coop/sd/design/boards)
- smichel17: what's the context/message being conveyed here?
- MSiep: wolftune had asked whether a US I started was ready for mockup
- smichel17: in that case, if you assign it yourself then it shows you're working on it, until you assign it to someone else
- wolftune: maybe the "Doing" label still has value as well?
- MSiep: there's the risk of forgetting to unmark "Doing", which could be misleading, sounds like having it assigned to me is okay
- wolftune: ok, plus trial-and-error and iterate
- NEXT STEP: n/a

## Reflections, general new process
- wolftune: I wanted to give a chance for a round-robin to make sure everyone's on the same page. no need to spend too much time on it
- seeing as it's been a while since we have chreekat, mray and MSiep present
- mray: it's a new process, I don't see problems so far, creating mockups at the moment
- MSiep: it's fine here as well
- chreekat: I haven't been involved in the process, no comments yet
- NEXT STEP: n/a

## Update on governance, next steps
- wolftune: we have a better starting point, but needs work still. elect secretary and lead for website circle, have unfilled roles figured out, etc.
- wolftune: easiest way is to have people look at the [current governance
  docs](https://git.snowdrift.coop/sd/governance), raise questions or tensions
  on irc or in issues
- NEXT STEP: everyone check out the updated docs and bring any questions/concerns to irc or other casual context (if general/vague/unclear) or a gitlab issue (if specific)

## Use of mailing list currently
- Salt: tangentially related to Discourse and an ongoing topic
- wolftune: I heard from someone saying they haven't received emails in a long time, asking if channel has changed?
- should we send out an announcement to the discuss list about things being discussed
- mray: why isn't Discourse running properly yet? I don't think it's a good idea to send out "sign of life" emails
- wolftune: it is, but it still needs testing
- Salt: I agree in part with mray, though we could send out a ML announcement this weekend about the status of ML, why the ML has been quiet
- (because we're working in the different git repos, and moving to <https://community.snowdrift.coop/>)
- NEXT STEP: Make sure discourse functionality is all set up the way we want
- NEXT STEP: Add some countent to discourse ourselves
- NEXT STEP: Salt/wolftune draft and sends ML announcement on status of ML and Discourse and refer to issues at git.snowdrift.coop also
- NEXT STEP: Update lists.snowdrift.coop ML repo so that it says it's deprecated
  and links to community.snowdrift.coop. Assigned: wolftune
- NEXT STEP: create discourse topic about moving from mailman to discourse, Assigned: smichel17

## Meetings with only relevant roles
- wolftune: circle tactical meetings so only the relevant roles need to come?
- chreekat: I'd like to have a website circle at 2 PM EST tomorrow
- smichel17: proposal to have weekly, website and outreach circle meetings. monthly, general, all-hands-on-board meeting?
- wolftune: I oppose, better to have more meetings
- Salt: I also think it's important to have cohesive meetings
- NEXT STEP: Circle Leads (until secretaries are elected) schedule tactical meetings; Website tactical → chreekat; Outreach tactical → wolftune


# Carry-overs

```
## LFNW
- NEXT STEPS: reach out to OSI about LFNW table etc.
Assigned: wolftune

## Board: organise the process, reach out to people
- NEXT STEPS:
- plan board meeting, set available time, contact everyone
- gather list of resources and advisors to contact
- reach out to advisors with reasonable set of questions
 Assigned: wolftune

## Roadmap
- NEXT STEPS: make archive wiki page with links to old discussions that might be useful at some point, so wolftune will be happy (smichel17)
Assigned: wolftune, smichel17

## Discourse
- NEXT STEPS: create discourse topic about moving from mailman to discourse (and send a link to it to the relevant mailing list?)
Assigned: smichel17

## Legal email, discussion, etc. from <https://wiki.snowdrift.coop/resources/meetings/2017/2017-08-11-meeting#next-steps-3>
Assigned: wolftune

## Describe a process for how we’ll use CiviCRM to enter and tag prospective first projects
Assigned: Salt

## Alpha announce / blog post
- NEXT STEPS: go over alpha milestone list on taiga, clarify prereqs (analytics, usability testing, etc.)
Assigned: wolftune writes blog post

## Recruit projects
- NEXT STEPS: consolidate our list of potential partner projects
Assigned: wolftune and/or Salt
```
