# Meeting —  January 16, 2017

Attendees: iko, jazzyeagle, mray, msiep, salt, smichel17, wolftune

## Checkin

- iko: almost finished dockerizing gitit, just a few details to work out
- jazzyeagle: got a MR very close to merge, finding a few details to change,
  but so far so good
- mray: johannes is onboard, we're waiting for audio from wolftune
- msiep: drafted something in ethercalc to show everyone, for people to find
  time to meet
- salt: available to answer questions and will set aside some time to work on
  snowdrift, but busy working on contract project until done
- smichel17: back on campus and settling in, I will have the next 2 days open
  until school starts
- wolftune: off work and sharing babysitting duties today, and I'm planning to
  do audio recording for video today

---

## Agenda

- Shared EtherCalc for team availability (MSiep)
- Update on intro video? (Salt)
- Governance (smichel17)
- Title case vs sentence case on pages (JazzyEagle)
- CiviCRM (Salt)


## Shared EtherCalc for team availability (MSiep)

- MSiep: <https://oasis.sandstorm.io/shared/bCyvQ4-KlB35zxSri9ggZC8ZHW9S4y4Erwxw9vIQ0e7>
- on the left, set the time zone relative to UTC in the yellow cell next to
  "Show as time zone: UTC"
- Person column - a person's availability across rows of time slots
- use the yellow cells above the person column to select person
- Group column - percentage of people selected are available
- smichel17: looks good. the only issue would be if multiple people were using
  it at once
- salt: agreed, this looks really cool
- wolftune: unfortunately whenisgood.net isn't open source, but you can take a
  look at it as useful reference. a while back I contacted them about
  potentially releasing the app as open source, they were receptive to the idea
  but it hasn't happened yet
- smichel17: I'm honestly ready to use this spreadsheet without any further
  discussion and iterate if needed.
- mray: have you looked at <https://dudle.inf.tu-dresden.de/> ?
- wolftune: it's not as comfortable as this. the doodle-style approach is more
  clunky
- unless the dudle one has updated recently, the best open source one is
  framadate, and it's still bad
- salt: thank you very much for working on this, msiep

**Next step: msiep to finalise the setup using multiple sheets and will notify
team when it's ready to add availability**


## Update on intro video? (Salt)

- see <https://tree.taiga.io/project/snowdrift-outreach/us/512>

**Next step: wolftune gets the audio recording out**


## Governance

- see <https://tree.taiga.io/project/snowdrift-outreach/us/1011>
- smichel17: resuming discussion of governance docs from last week
  <https://git.snowdrift.coop/smichel17/governance/tree/overhaul>
- this is an overview of the governance structure
- we can review changes since last week
- salt: look at the role purposes in particular
- if you notice that's unfilled and it's actually currently filled, please
  mention it (as well as reverse scenario)
- we got rid of secondary Ops Circle and moved the roles previously within to
  Outreach
- added cross-links between Circles
- website circle can be visualised as, if we were hiring people externally to
  build the website, these are the people who would be in the team
- jazzyeagle: in a lot of orgs I've seen, positions get separated or merged
  often, it's not necessarily a bad thing
- in my opinion what is a bad thing is focusing on roles that we're not going
  to fill for a long time
- we should get this working for us, then add more later when needed
- my curiosity is, is this really the most important thing we could be doing,
  is this the best use of our time?
- salt: I think it's worthwhile to have this. we decided a while back to go
  with Holacracy. without a solid org chart, it can't really work because no
  one knows what they do
- wolftune: there's value in me being able to say, I'm wearing x hat when I'm
  making a comment or decision, to help with clarity
- smichel17: having this explicit gives people confidence to go ahead and do
  things without having to check with wolftune for everything
- this page is about how you work. the idea of accountabilities is a guide to
  how to fulfill a role
- wolftune: can we do a concrete example? e.g. title case vs. sentence case
  question
- smichel17: sentence case is a design/ux issue, so we look starting at the GCC
- inside the Website Circle, see it's an Interaction Design role. then click on
  it to see policies
- salt: if it's the first time and the question isn't covered in existing
  policies, contact someone in that role, get answer then a person in the role
  updates the policy
- policies will be linked from the role accountabilities page
- e.g.
  <https://git.snowdrift.coop/smichel17/governance/blob/overhaul/GCC/Website%20Circle/Interaction%20Design.md>
- wolftune: it would be mentioned in something like a style guide
- salt: the way I see it, there would be a style guide for copywriters
- jazzyeagle: in this case, there are two people filling the role, do I have to
  make sure that both are okay with it?
- smichel17: if there's multiple people in a role, but later there should be an
  indication of the person looking after which roles


## Title case vs sentence case on pages (JazzyEagle)

- salt: I think jazzyeagle got the answer on how to ask that question, which is more important


## CiviCRM (Salt)

- salt: civicrm is up and usable, but just doesn't have contributions worked
  out. unfortunately, that takes time to research
- wolftune: in the meantime, we have some people who donated recently, what
  should I do?
- salt: log into civicrm, add their name, write an admin note that they donated
  x amount of money

**Next step: Salt and wolftune walk through this process together**



