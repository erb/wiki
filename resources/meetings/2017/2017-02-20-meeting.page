
# Meeting — February 20, 2017

Attendees: iko, mray, MSiep, Salt, wolftune, Hayden, Johannes

## Checkin

- iko, msiep: nothing to report this week
- mray: made some progress with video stuff, things are looking fine
- salt: I didn't get to much last week, but I'm scheduling the month out
- I'm planning for upcoming conference talks and getting discourse working, so
  it's moving up the priority queue
- I should get the docker thing done, but having scheduling issues
- wolftune: I'm here with Hayden, a CS major who's interested in helping with
  snowdrift.
- also looking to prioritise backlog
- Hayden: I'm still trying to see where things are at and where I can fit
  myself
- Johannes: I'm new to snowdrift and will be working on the video
- I already started on the animatics, have some ideas on how to go about it

---

## Agenda

- Blog status (mray)
- Governance: design needs review and the kanban (mray)
- Scheduling governance meeting to clean up dual-held roles
- Upcoming conferences (Salt)
- Team availability (Salt)
- Video script issue (mray)


## Blog status (mray)

- mray: I just wanted to bring this up as we're kind of bad at communicating
  our current status
- I think we need to put up news more often. from the outside it seems to be an
  inactive project
- wolftune: agreed
- salt: last time, salt and I were going to decide what blog software to use,
  but that didn't happen
- wolftune, can we schedule a time to discuss this?
- wolftune: we can also consider adding something to the wiki for now

**Next step: salt and wolftune schedule meeting to discuss blog**


## Governance: design needs review and the kanban (mray)

- mray: <https://git.snowdrift.coop/sd/governance/issues/12#note_653>
- I feel it's holding my feedback and not in control of the workflow
- it should be resolved soon. I was hoping Stephen will be on today and a
  solution will likely need his involvement
- salt: I sympathise with that. maybe we can set a time when he is available
  and anyone interested can talk about it
- I recommend using the team availability chart, his times and yours are listed
  there

**Next step: mray sends out message to schedule meeting with smichel17**


## Scheduling governance meeting to clean up dual-held roles

- salt: I think this is the same as the previous topic and could be scheduled
  at the same time
- either back-to-back meetings or separate times. mray, do you mind including
  it in the announcement?
- mray: I don't mind, I'll send it


## Upcoming conferences (Salt)

- salt: I know there are 2 wiki pages, we were going to work on them last week
  but didn't get to it
- <https://wiki.snowdrift.coop/communications/publicity#conferences>
- <https://wiki.snowdrift.coop/community/events>
- I wondered about a recent email (it was either at OSCON or SCaLE?) about
  getting a booth at the conference
- wolftune: I think it's too late to get a booth, including things need to be
  done
- since we're an OSI incubator project, if you want to go to OSCON, you could
  potentially talk to OSI rep and volunteer, and they can probably get you in
- salt: OSCON starts on Monday, LinuxFest ends on Sunday. there's a schedule
  conflict (Linux Fest off by usual time by 2 weeks)
- I'm waiting to hear back from Linux Fest about table. I'm also hosting
  indiewebcamp a few days before Linux Fest.
- the first thing coming up is LibrePlanet, and I'd like to bring up materials
  for the conference. any other snowdrift plans?
- wolftune: you should connect with Stephen
- maybe you could also send out a notification to people you'll be available at
  a certain time/date and meet up
- mray: I have been looking at reveal.js, would that be something useful for
  you, to build some slides?
- salt: I'll take a look, thanks


## Team availability (Salt)

- salt: <https://oasis.sandstorm.io/grain/5dZHew9EgNfzTvm5BuXtNk>
- we were looking for a way to get a shared calendar
- msiep came up with a team availability schedule schedule, so this is a
  reminder for people to add their availability and make use of it
- I noticed jazzyeagle hadn't signed out of it, I've pinged him as a first
  step. what should I do?
- msiep: yeah, there's not much that can be done. if you're sure they're not
  using it anymore, you can insert your name and switch the timezone


## Video script issue (mray)

- johannes: I already started last week to make an animatic of the video. I
  noticed the beginning was quite good and worked really well
- I see some patrons starting at sentence 6, "1000 patrons …", it's a bit
  confusing
- wolftune: I think it's fine to just add back the words along the lines of "1
  dollar per 1000 patrons" to make it clearer
- mray: maybe you can look at johannes' animatics to see
- I previously underestimated the problem. I thought the script would be more
  important, but the audio and video are equally important
- johannes: I wonder if it would be better to focus on what is different from
  other crowdfunding platforms instead of a specific example
- there's a lot of numbers to take in
- wolftune: I think talking mainly about the benefits is a good direction
- I also see the value of showing what happens when more people join, and
  showing a visual along a graph
- the concrete example helps people visualise it
- mray: this is not representative of what we're doing. I really suggest
  looking at the animatics
- there are multiple sets of numbers and changing numbers through time, which
  are too much for people to take in a short video
- wolftune: I think the script and visual need to be developed symbiotically
  (and I had a feeling previously it would be the case)
- johannes, let me give you some general concepts, to see what your ideas are
  for the final visuals:
- mutual assurance - knowing when people will donate when they do.
- sustainable memberships - ongoing payment and not a lump sum
- the idea that the system we built is designed for the challenges that public
  goods face, it's meant for those needs that public goods have
- the reason it isn't written like that is because there are a lot of
  high-level, jargon-y words
- we can talk more about this after the meeting
- mray: on that note, I would like to get in touch with johannes after the
  meeting to show stuff that can be used

