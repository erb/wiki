---
title: Strategy
categories: project-management
...

This page presents general advice for project strategies with Snowdrift.coop examples for reference.

## Envision the dream

The *overall* vision for Snowdrift.coop is described alongside our [mission statement](/about/mission).

The vision will change only if we change our values. Given the vision, we then work to determine the most realistic and practical path toward our ideal. Our strategy is determined by proposing plans and then questioning and critiquing them — all with the vision in mind as our ideal.

### Defining projects

Guidelines for working on individual projects:

#. Clarify the problem at hand
#. Pick a time-frame for focus
#. Identify what is working well already that we want to sustain and reinforce
#. Envision the final result: be bold, sincere, personal, ideal
#. Hash out plans with proposals and critiques, anticipate challenges
#. Determine next steps with specific assignments
#. Document everything clearly
#. Get outside feedback

## Overall Snowdrift.coop strategy

* **Problem:** Proprietary non-rival products dominate in most areas and keep getting funded, so the general public doesn't have adequate power in the market to promote FLO projects that better respect freedom and collaboration. [Existing funding mechanisms for FLO projects](/about/existing-mechanisms) are not adequate.

* **Solution:** Our [matching patronage system](/about/mechanism) will provide incentive to support projects even when access is not restricted.

* **Users:** FLO projects and their community patrons and supporters

* **Our unique value:** Besides our matching patronage mechanism, we are ourselves 100% FLO and are a mission-driven non-profit co-op. We also offer many other [tools](/archives/general-management/services) and [advantages](/about/advantages).

* **Outreach:** Start by connecting with existing FLO projects and supporters and build out from there. See our list of [partners](/community/partners) to engage.

* **Revenue:** We will pass on fees for user transactions so user activity is not a direct expense. Otherwise, we will fund ourselves as a project within our own system.

* **Costs:** Development, management, promotion, co-op governance costs; legal costs, server expenses. *Need to specify details further.*

* **Metrics:** As we build the site and get test stats, we can construct tests with variations in the formula, the presentation, and elsewhere.

## Game plan for roll-out

* Get launched (after fund-drive, legal finalization, tech all ready)
* Get projects signed up
* Encourage projects to spread the message that their work is *not* gratis really
    * "How can this be available FREE?? Answer: because the community works to support this, and we need YOUR help to keep going"
    * Focus on converting more general people, not just the insiders
    * Like in shareware, suggest that while you *may* use the works freely, you *ought* to do your part and become a patron, not because you are generous, but because this is the expected thing; otherwise you are being a freerider (which is fine if you truly can't afford to help!)
    * Emphasize that the *reason* the project exists is because the patrons and volunteers support it
    * And when YOU pledge, the existing patrons will add MORE to match you, so PLEDGE TODAY
    * Also emphasize to each patron when they pledge that they should then go recruit other patrons
