---
title: User Profile — Chad Perrin
toc: false
...

## Personal Statement

I’m deeply interested in subjects like ethical theory, self improvement, and 
technological advancement, especially where they intersect. Books, computers, 
and opportunities to learn have always held very strong attraction for me. I 
have interests all over the place, really, but one day I realized there was a 
single thread running through most of it: philosophy, in the classical sense of 
the term.  
  
As a philosopher (among other things), all it can take to get me to realign my 
thoughts on some matter of importance sometimes is to ask me an honest 
question. A great example of that was when a friend of mine, at the end of the 
very day we first met, asked me if there was any real, fundamental 
justification for copyright at all. Over the next several years, this question 
led me to examine the premises for basic concepts of ethics all over again, 
leading me ultimately to the fact that, no, there did not in fact appear to be 
any reasonable justification for copyright at all. Quite the contrary, in fact. 
I’ve elided the details here (for mostly for reasons of brevity) though I have 
elaborated on them elsewhere.  
  
Business models that rely on copyright are, to me, pretty obviously nonstarters 
for those who wish to live an ethical life. In addition to that, though, the 
ultimate unenforceability of copyright in any meaningful, universal way 
indicates that the shrink-wrapped widget sales business model central to 
assumptions about the importance of copyright might in fact be the single 
stupidest, most unavoidably doomed business model currently in use for 
intangibles such as the writings of novelists, software developers, and 
musicians.  
