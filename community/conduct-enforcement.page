---
title: Snowdrift.coop Conduct Enforcement Procedures
categories: community, honor
...

Our [Code of Conduct](conduct) (CoC) and [Terms of Service](https://snowdrift.coop/terms) specify the rules participants must follow at Snowdrift.coop.

We do *not* enforce the [project honor code](/project-reqs/honor-projects) and the [user honor code](honor-users) which serve as less formal *guides* to further encourage the best behavior.

## Goals of enforcement

Our top priority is to keep all regular discussion in line with the CoC so that everyone feels welcome and safe. So, we specify *distinct* spaces for enforcement/resolution procedures. Accusations of CoC violation should never occur within the regular discussion contexts.^[When issues get hashed out in the court of public opinion without adequate structure, we see all manner of dogpiling, factioning, signaling, and shaming — all escalating conflicts and derailing any constructive discussion.]

Within enforcement procedures, we emphasize [restorative and transformative justice](values#restorative-and-transformative-justice). That includes giving people a chance to correct mistakes and learn from them. Most issues get resolved through simply clearing up misunderstandings or fixing the immediate problem(s). Further punishment or limiting of community members will only happen for egregious and/or persistent violations.

## Procedures and tools

If you see a violation, your options (any or all) are:

- flag problem post(s) in our forum
- discuss issues directly with other participants (perhaps with facilitation) in
  appropriate spaces
- report the concern to moderators

### Flagging forum posts

Within the forum, we have a separate [guide to flagging posts](https://community.snowdrift.coop/t/guide-to-flagging-posts/879)

In the case of a Code of Conduct violation, a post should be flagged by whoever first notices the problem (whether or not it affects them personally).

Flagged posts get hidden from public view, and the author receives a system-generated message with an invitation to edit and repost. If they repost, the flagger(s) will get notified to check if the fix is adequate.^[We hope to eventually have a system that will allow flaggers to mark the specific CoC violation, include suggestions right in the flagging message, and several other enhancements. We plan to describe such ideas in depth some day along with a call for developers to implement such features in Discourse and other tools.]

### What to do if your post is flagged

Remember that most issues require human judgment. For example, if someone feels insulted, that feeling is real even if it's based on a misunderstanding. Since editing a post clears the flag, see if there's a simple fix that will resolve everything. Treat each case as a chance to improve, even if you don't believe you really violated the CoC. If you're not sure how to fix a post, believe the flagging is wrong or in bad faith, or you are feeling discouraged from participating because of it, contact moderators for help.

### Discussing issues

Whether in conjunction with flagging or on its own, any participant may send a private-message to others and/or to moderators.

Along with a flagging, for example, you could offer more specifics about a violation and/or suggestions for fixing the problem. If comfortable, send that directly. To remain anonymous, send the message to moderators and ask them to forward it to the violator anonymously.

A good template for such feedback:

> I'm writing regarding your post [link]. Though you may have intended otherwise, one interpretation is  ______. I feel that does not follow [specific relevant portion(s) of CoC]. To fix this, consider [suggestion(s)].

Before any extended discussion of an issue, get consensus from all participants on whether to discuss privately or publicly (as long as away from normal topical discussion); whether to use forum messages, chat on IRC or Matrix, or live audio/video tool; and whether to get facilitation help.

Use our [healthy communication guide](honor-users) to help keep discussions productive and respectful.

### Reporting to moderators

Currently, our forum alerts moderators to any flagging. The flagging interface also has a separate "something else" option to send a message to moderators. That can be used alongside other flagging options.

Any other contact method may also be used to reach moderators with any concerns. Do not hesitate to report any and all issues. We would rather err toward excessive reporting than to see any harmful situation go unaddressed. When feasible, we do appreciate screenshots, links, and any other details that will help us understand the situation.

To report a staff member, use our [staff listing](https://community.snowdrift.coop/about) to find a different staff member to contact directly.

#### Handling reports

We will do our best to respond to reports within 24 hours, understanding that people live in different time zones, have varied schedules, etc.

All reports to staff (and all flagging in our forum) are held in confidence. Unless we have consent otherwise, we will not reveal the identity of a reporter to a violator or to any non-staff member.

For each case handled by moderators, we aim for consensus among participants about whether and how to announce anything public about the outcome or the situation. Where consensus fails, we will use our best judgment to balance privacy for participants with the need for transparency and clarity for community health.

#### Becoming a moderator

We invite people to become moderators after they have shown exceptional compassion and understanding in their engagements with the community.

### Conduct violations outside of the forum

Outside of our forum, we have no simple flagging tool for conduct violations. So, the following applies to our issue tracker, our live chat (Matrix.org/IRC), in-person events, social media, and any other interactions around Snowdrift.coop:

* Whenever an alleged conduct violation occurs, anyone who observes it should *always* address it in some form, with the specifics up to the best judgment and comfort of the observer(s).
* Addressing a violation could include any of these approaches (working to follow the Code of Conduct yourself in the process):
    * Message or contact the violator privately, asking for them to address the issue
    * With efforts to do so in ways that do not otherwise derail discussion, publicly mention the issue and ask the violator to fix the problem
    * Contact Snowdrift.coop staff to request assistance, describing the violation as clearly as possible

In contexts where we cannot delete or edit after the fact, resolutions may emphasize making a public statement of retraction/clarification and/or otherwise apologizing and making amends.

Note that while Matrix posts can be redacted, the IRC side of our chat room will remain.

### Staff actions in extreme cases

We will do our best to get back to you within 24 hours (understanding that people live in different time zones, have different schedules, etc.).


While we hope to never need to do more than assist everyone in maintaining healthy, respectful communication, we *will* reduce the privileges or otherwise remove or ban people from the community *if*:

* they exhibit repeated, persistent bad faith violations of the Code of Conduct
* they are toxic enough that their inclusion threatens to drive away other community members who themselves display good faith
    * note that because including diverse perspectives is among our core values, we will give some extra deference to retaining minority perspectives (meaning those perspectives which are minority ones in *our* community specifically)^[Of course, the one form of diversity we do *not* value is opinions that go against our Code of Conduct, mission, or values. For example, we have no wish to include bigots just because they add diversity to the opinions represented.]

## Flagging project violations

All projects must meet the core [project requirements](/project-reqs) and provide a report about where they stand on the items from the [project guidelines](honor-projects).

If you find that a project does not meet (or no longer meets) the requirements or is *misrepresenting* status on any other issues, [contact the staff](https://snowdrift.coop/contact) and specify the exact concern. We will give projects a period of time (to be decided) to rectify or otherwise respond to the complaint. If the time passes with no response, we may decide to either public mark the violation or suspend the project entirely. Unless we see a pattern of repeated egregious violations, any project that addresses concerns will be welcome to continue in good standing.
