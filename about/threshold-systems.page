---
title: Issues with campaign crowdfunding
categories: communications
...

**The standard crowdfunding mechanism involves one-time fund-drives with strict goals and arbitrary deadlines.** These campaigns can help bring people together and often shows greater success than simple donation requests. However, campaigns are often costly to organize and promote, run a significant risk of failure, and provide little sustainability or accountability.

## Assurance contracts help achieve critical mass

The solution to the [Snowdrift Dilemma](snowdrift-dilemma) involves an [*assurance contract*](http://en.wikipedia.org/wiki/Assurance_contract) — an agreement that we will all do our part if others do their part. Without such an agreement, few projects develop a critical mass of supporters.

## Standard crowdfunding relies on a set threshold

Most crowdfunding systems (e.g. Kickstarter) provide assurance of community support through a [*threshold pledge system*](http://en.wikipedia.org/wiki/Threshold_pledge_system) where nobody donates unless the total funding reaches the predetermined goal.

### Only partial assurance

A hard threshold assures that people work together somewhat (unless an angel donor pledges the whole goal amount). Of course, some of the funding would have come through without any assurance contract. And once a campaign hits its goal, extra donations no longer have any mutual assurance.

### Difficulty setting the threshold

Setting a threshold involves some risk analysis and guesswork. When a project can manage with only modest funding, they may set their campaign threshold as low as possible to reduce the risk of failure. Of course, a low threshold approaches no threshold. The artificial hard line between success and failure is inherently problematic.[^gaming]

[^gaming]: **Threshold systems are easy to game (and not get caught)**. A project can set an ambitious goal to encourage donors and to provide supposed assurance of widespread community support. But then when success looks unlikely, the project can borrow money to cover the difference (perhaps via a confederate donor). The campaign gets marked a success, and they pay back the loan after receiving the funds. The same gaming could also boost a drive early on to artificially enhance the appearance of popularity and community support. Perhaps a high percentage of campaigns get manipulated in this way. We haven't seen any research into this issue, but we know of multiple cases.

    **Gaming a threshold goal is analogous getting a bank loan approved on condition that the borrower have a down-payment, but the borrower then takes out a separate loan to cover the down-payment** (and then uses the first loan to immediately pay off the second). *If* the project still delivers results with their reduced funding, perhaps there's no real harm, but the dishonesty and false pretense is still troublesome. Incidentally, some platform policies help address this; for campaigns that reach over 80% but less than 100% of their goal, the Danish site Voordekunst specially offers a go-ahead-anyway if the project makes a statement about how they can still produce something worthwhile with less funding.

    Gaming of crowdmatching by having confederate patrons (or just project team members themselves) pledge is probably less of an issue. No single patron can have a big effect, so the only way to game crowdmatching this way would be with a large quantity of confederate patron accounts. That would be much more complex (and likely easier to catch) than just a single large donor to a threshold campaign. And if a team of 5 (for example) simply pledges openly to their own project, the extra half-cent that costs other patrons will not be a problem. Unlike the down-payment analogy, **a self-pledge in crowdmatching is closer to the common practice of seeding a tip jar with a few starting dollars** to give the impression of social reinforcement.

### Threshold systems require time-limits

A threshold system needs a set point where a project must accept failure. Without a deadline, a fund drive could stretch on and on, hoping to finally get enough pledges. That would waste everyone's time. We cannot rely on donors to make good on pledges made many months or years earlier.

## Result: high-pressure campaign

Combining a funding threshold and a short time-limit leads to a high-energy, marketing-focused campaign. In some cases, a campaign may galvanize the project, but enormous investments in campaigning can steal resources from the project itself. Many heavily marketed campaigns compete for attention and amplify the volume of advertising and hype. This also creates a barrier to entry for projects without the resources or inclination to mount such campaigns. Success may go to the best marketers rather than the most deserving projects.

## Reliance on rewards

Threshold assurance alone often proves inadequate, so campaigns rely on rewards and other incentives. In many cases, such rewards veer toward a simple consumer exchange. The decision to donate may become less about the project as a social initiative and more about choosing rewards. The consumer mentality became so prevalent at Kickstarter that they had to adjust their policies and issue a statement that Kickstarter is not a store.^[<http://www.kickstarter.com/blog/kickstarter-is-not-a-store>] The need to produce artificial rewards can also prompt projects to choose proprietary terms so they can offer special patrons-only access.

## Lack of accountability and costs of failure

When a threshold fund-drive succeeds, projects typically receive all the funds in a lump sum up front with no guarantee that they will actually deliver results.

## Costs of failure

Beyond wasting time and money, a failed campaign may damage a project's reputation.

## Poor fit for public goods

Many projects need ongoing long-term funding rather than just startup capital. Some public goods are just practical and not that exciting, and they may have little to offer in the way of rivalrous goods as perks for donating. The high-energy, one-time fund drive can be a poor fit.

**Most crowdfunding sites have no requirement for free/libre/open release of products.** The values of having exclusive rewards and later monetizing the products as [exclusive club goods](economics) discourages crowdfunding campaigns from releasing as public goods. With most crowdfunded projects staying proprietary, the overall crowdfunding trend might work *against* the public commons more than it helps.

Of course, some public goods projects have run successful threshold campaigns. They make sense for start-up funds or for significant one-time initiatives. For anyone wanting to still run such campaigns, see our summary of [other crowdfunding sites](/market-research/other-crowdfunding) for our research into the best platforms.

## Snowdrift.coop is different

Snowdrift.coop does not aim to provide startup funds. Instead, **we aim to provide long-term sustainable funding** using our [new type of assurance contract](mechanism) with no arbitrary deadlines, no arbitrary reward-levels, and no arbitrary cut-off points. Our system addresses the public-goods funding dilemma while being flexible, long-term, easily adapted to many different types of projects, and especially designed for free/libre/open works.
