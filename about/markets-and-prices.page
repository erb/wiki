---
title: Market Prices
categories: communications
...

## Market Pressures, Prices, and Donation Levels

Many people believe a simple narrative that prices get set by the "invisible hand" of the market and thus somehow make sense. In reality, what may seem sensible is more based in our familiarity with somewhat arbitrary social norms. The same issues apply to giving charitable donations. Few of us really have any truly rational budget or otherwise reasonable sense for how much we can or should give to causes and projects we support.

### Beyond simple supply and demand

Market pressures *help* to determine prices, but even pure commodities get manipulated by speculation, advertising, regulation, and actions by powerful oligopolies and monopolies.

#### Psychology of pricing

Most goods and services are not directly-comparable commodities (and sellers of commodities do all they can to brand their goods and make them seem unique). Often, a higher price for an item leads consumers to treat it as more valuable. A very high price that excludes poorer consumers can make a product into a symbol of wealth regardless of the actual cost or value of the item otherwise. In other cases, goods sell below-cost as part of complex marketing strategies.

#### Price is not value

Admittedly, prices do reflect *something* of the basic costs involved in providing a particular good or service, but the more arbitrary aspects of price may overwhelm the fundamentals. Most free/libre/open projects today provide substantial value and also have substantial *development* costs, yet their prices generally remain at zero. Obviously, with near-zero *redistribution* costs, there is no good way to enforce charge-for-access aside from using artificial restrictions. For more on this, see our page about the [economics of non-scarce goods](economics).

#### No easy answers

However prices are set, the idea of a "correct" price is largely an illusion. Even with great effort and research, pricing involves so many factors that we almost always use some guesswork. And prices reflect the values of those setting them. Some people set prices to maximize their profits above all else, while others remain uncomfortable with charging predatory or exclusionary prices.

### Supply-side vs. demand-side power

In today's supply-side system, producers set prices, and consumers only have the option to buy or not. We see far fewer demand-driven markets where consumers have the power to set their offers to purchase products or services. Suppliers today also tend to be better organized in large corporations and trade groups while we have comparatively fewer, smaller, and weaker consumer organizations.

### Choosing donation amounts

So, in most cases, consumers don't need to even think about setting prices. We simply choose whether to accept a given price. However, voluntary donations are almost entirely demand-side, and donors must choose how much to give. And with that power comes extra responsibility and decision-making.

### Balancing interests and providing sustainable funding

At Snowdrift.coop, we don't claim to have all the answers, but we know that prices and donation amounts get set through social norms and other complex interactions. Instead of fighting against that trend, we're embracing a market-driven approach while setting up the market so it will maximize the positive effect for overall social good.

Our system provides a framework for coordinating patrons and projects to provide effective support for creative work. Our pledge formula helps the community to collectively determine fair and effective share values that meet the needs of the projects we support. Of course, we may adapt our precise formula as things go, and any decisions after initial launch will be made through our democratic cooperative system. To avoid overly favoring either supply-side or demand-side, our [multistakeholder co-op structure](co-op) balances the interests of both projects and patrons.
