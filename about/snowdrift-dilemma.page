---
title: The Snowdrift Dilemma
categories: communications
...

The name *Snowdrift.coop* refers to the *Snowdrift Dilemma*, a metaphor from game theory (the study of strategic decision making).

Such games generally assume all participants to be self-interested rational actors, and we know *you* aren't like that, but the model remains insightful despite its [limitations](#snowdrift.coop-goes-further). For a  great interactive intro to this type of game theory, check out Nicky Case's [Evolution of Trust](http://ncase.me/trust).

The snowdrift dilemma is particularly relevant to freeriding and public goods.

## A snowdrift blocks the road

In a small neighborhood after a winter storm, a big snowdrift blocks the road. It's too much for one person to easily clear. Everyone has other things to do.[^micro-macro]

## Will you help clear it?

<div style="text-align: center;">
![](/assets/snowdrift-road-eunice.png)
</div>

If you go out to shovel right away, others may freeride and you'll end updoing all or most of the work on your own. If you *knew* others would help, you might cooperate. But you might be happy to freeride yourself and let others clear the road without you.

## Who gets started first?

The rational strategy in the snowdrift dilemma: wait and see what others choose before you decide whether to help. Of course,  if everyone does that, nothing happens while we all just wait for everyone else.[^prisoners-dilemma]

Sometimes, someone can't wait any longer and ends up shoveling alone. In that case, the snowdrift may get cleared (maybe just enough to barely get by), but the one shoveler took an unfair burden at great personal cost.

## Ongoing iteration *can* help build trust

When a dilemma like this is a one-time case, we have no basis to trust others or care about building trust. But if we know we'll see the same situation again, we have more incentive to build trusting relationships. So, recurring dilemmas have a better chance to work out than one-off cases.

## Voluntary contributions dilemma

Fundraising for public can be a snowdrift-type dilemma. Everyone gets the results whether or not they contribute. Self-interested players will tend to freeride. Projects can have tons of potential supporters but still fail because people don't want to be in a small group that carries all the burden. A few self-sacrificing supporters may donate anyway, but the project won't reach its potential.

### One-time crowdfunding campaigns use thresholds to partly address the dilemma

Platforms like Kickstarter address the coordination problem by having donors pledge on the condition that everyone together reaches a preset fundraising goal. This threshold assurance is a major *factor* in the successful crowdfunding boom. However, there are several [problems with threshold campaigns](threshold-systems) that make them an unsustainable and inadequte solution.

### Snowdrift.coop solves the dilemma with ongoing crowdmatching

Unlike one-time crowdfunding campaigns, **[crowdmatching](mechanism) provides both mutual assurance *and* sustained, ongoing support.** This reduces everyone's risk and maximizes the collective impact and the significance of each pledge.

#### Addressing social psychology

Real people are more complex than the rational self-interest model of classical economic game theories. We have complex social motivations like honor, altruism, guilt, and revenge.

Beyond crowdmatching itself, Snowdrift.coop also aims to address [social psychological factors](psychology) to build a resilient community of public goods supporters.

[^micro-macro]: Usually the snowdrift game assumes just two actors. We've reframed it to describe a larger community (a neighborhood) because we focus on the larger scale. Although the dilemma *does* translate well, different scales change and complicate things. Micro ≠ macro. We can't predict the expansion of a gas by studying only the isolated behavior of single atoms. Similarly, we cannot simply extrapolate from small scale game theory or economic behavior and thus know fully how macro-level patterns work (let alone assume that we have fully and accurately described the micro level!). For our purposes, the game theory is just one way to frame the conversation. It is far from the complete picture.

[^prisoners-dilemma]: The snowdrift dilemma is similar to the better-known *[Prisoner's Dilemma](https://en.wikipedia.org/wiki/Prisoner%27s_dilemma)*. By comparison, the Prisoner's Dilemma is more intractable. **In the Snowdrift Dilemma, the rational action depends on what others decide (so it's a waiting game, wanting others to act first). But in the Prisoner's Dilemma, defecting (i.e. deciding to *not* cooperate) is *always* the rational choice** for one-time, non-iterated games.

    For those unfamiliar with the original Prisoner's Dilemma:

    Two prisoners have been charged with a crime. They are separated and each asked to confess. If both confess, they will both be convicted. If they both claim innocence, they will face a lighter charge. If one confesses and the other refuses, the one who confessed will go free; and the other prisoner will then get an extra harsh sentence.

    So, as a player in the prisoner's dilemma:

    * If the other prisoner stays silent, you can confess and go free.
    * If the other prisoner confesses, you had better confess as well — otherwise you'll get an extra harsh sentence.

    It doesn't matter what the other player chooses! You should confess *regardless*. You don't need to wait for the other to decide. So, two rational prisoners will always both confess in this game. Yet they would *both* be better off if they had both stayed silent!

    The only way out of the prisoner's dilemma is through both players acting pro-socially even at great personal risk. Or if we know that it's an iterative game that will repeat with no fixed end point, *then* we can see cooperation become a rational signal to the other player to encourage future cooperation.

    The Snowdrift Dilemma has more chance of cooperation even in single rounds. Thankfully, many real-life situations are more snowdrift-like than prisoneri-like, but the details vary from case to case. A public goods project that runs a risk of total failure due to insufficient resources can be like a prisoner's dilemma. With no guarantee of help from others, any one person risks totally wasting their contributions (whether in volunteering time and effort or as a patron donating funds). By contrast, a struggling (but not dying) project that just needs help to prosper works more like a snowdrift dilemma.

    For some academic research on the difference, see this [article at phys.org](http://phys.org/news111145481.html) which summarizes a study titled "Human cooperation in social dilemmas: comparing the Snowdrift game with the Prisoner’s Dilemma".
