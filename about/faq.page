---
title: Frequently Asked Questions
categories: communications
...

See [About Snowdrift.coop](/about) for basic explanations and links.

Don't see your question here? It must not get asked frequently enough. You can [contact us](https://snowdrift.coop/about) for clarification.

#### Your matching idea is great, but I'm worried about something…

We have considered many issues in our design. **If you have not yet, please fully read our [mechanism](mechanism) and [limits and growth](limits) pages**. Most likely, one of those pages addresses your concern. If not, please post a comment on one of the connected discussion boards. We can tweak the mechanism as we continue testing. As a co-op, the community will make long-term decisions about adjusting anything.

#### Your matching idea is stupid. Why don't you just let people donate whatever they want?

We aren't stopping anyone from using the plain old "donate" options already exist, but that approach clearly hasn't solved the FLO funding issues generally.

Without matching, we remain stuck in the [Snowdrift Dilemma](snowdrift-dilemma). It doesn't work to just ask each person to make the right decision. Beyond connections between each project and each patron, we envision a network of patrons all connected to *each other* to bring more power to the community. We don't believe any unilateral donation approach will succeed as well.

#### What does *Snowdrift* here mean?

The name comes from the [Snowdrift Dilemma](snowdrift-dilemma) in game theory. **How can we get everyone to cooperate to clear a snowdrift blocking the road?** The dilemma occurs whenever we need voluntary support for work that benefits the public generally.

The name does *not* refer to a snowball effect or something about how lots of little donations add up. We can certainly talk about how lots of people shoveling the snow goes fast (many hands make light work), but the snow in our metaphor is the obstacles we're working to clear.

#### FLO? Free/Libre/Open — *what?* Libre? That's not even English. Why does this have to be so complicated?

Yeah, we wish it were simpler. These issues have a long history, and it seems unlikely people will ever find a good consensus term.

*Libre* is the least ambiguous term — it means *liberty* as an adjective. *Free* and *Open* each have many different interpretations and lead to confusion, although each have their advocates. The best compromise simply includes all terms. See our page: [*What Does Free/Libre/Open Mean?*](free-libre-open)

#### Is Snowdrift.coop just for Linux stuff?

Snowdrift.coop supports all sorts of FLO projects including things like music and journalism that do not directly relate to computers.

We also support FLO software for any system, including proprietary systems like Windows and OS X. We want to maximize the percentage of FLO programs in every way we can. Ideally, FLO developers will also provide their works for GNU/Linux, and doing that is part of our [project honor ideals](/project-reqs). Of course, cross-platform software provides the smoothest transition for users moving to GNU/Linux.

#### Why limit to only FLO projects? Aren't you unnecessarily imposing an ideology on the system?

FLO works have particular issues as non-rivalrous public goods. Snowdrift.coop works to address those particular concerns. See more at [*Why Only Free/Libre/Open Projects?*](why-free)

#### Why restrict to only non-rivalrous goods instead of all public goods?

Our system might work for other sorts of public goods, but we want a clear focus right now. See [*Fundraising for Charities and Other Purposes*](other-fundraising).

#### Have you heard of X, Y, or Z other funding systems or ideas?

Probably. We have done extensive research. We describe general funding approaches at our page [*Other Funding Options for Non-scarce Projects*](existing-mechanisms). We have separate pages about the history and issues in different fields, as well as a summary with commentary about the many other crowdfunding websites, all of which can be found in the [Market Research](/market-research) section. If you review those pages and know of something missing, please tell us more!

#### What about Bitcoin? You should use that!

**We see Bitcoin as an implementation detail which we could eventually support.** We don't want to build a limited, Bitcoin-specific system. Today, Bitcoin remains too speculative and volatile to be the foundation for the sustainable economy we're working to build (not to mention extra legal questions). So, we plan to stick with dollars, but we hope to offer a way to make deposits with Bitcoins which we will then exchange for dollars automatically. If Bitcoin stabilizes and achieves far greater adoption, we will consider supporting it more directly.

#### What's the significance of being a non-profit co-op, and what about 501(c)(3) status?

First, we have initial incorporation under non-profit law in the state of Michigan, but we have not yet finalized our complete legal status. "Non-profit" means that 100% of our revenue must go toward serving our non-profit [mission](mission). We have no stock, no stock-holders, and no investors who would get any return (aside from getting the results of FLO works that we support, which everyone gets whether they donate or not). If income surpasses expenses in a given period, that income may be used to expand operations or provide a buffer in savings, but the co-op member-owners do not get dividends or other profit from the system.

The "co-op" part means that the members own the system and make decisions democratically. We abide by the general principles of cooperative organizations. See our [co-op](co-op) page for more.

Although we would like 501(c)(3) status, our scope may simply be too general to fit the limited boxes that the IRS defines. We are evaluating 501(c)(4). Having 501 status of any sort would mean tax-exempt (not owing taxes on net income). 501(c)(3) specifically refers to traditional charities, and that status would mean that donors to us can deduct their contributions from *their* taxes. Our [legal](/legal) page has discussion about these issues and our status.
