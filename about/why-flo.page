---
title: Why Only Free/Libre/Open Projects?
categories: communications
...

## FLO is our mission

Proprietary projects already have an effective funding mechanism: pay-for-access! Snowdrift.coop exists specifically to solve the problems that come up when we reject proprietary restrictions. In any case, a project has no business giving clients damaged goods. When community patrons fund the projects, the community should expect unrestricted access.

### Building on existing ideas {#derivatives}

**Access alone is not enough. Projects must be free of usage restrictions and open for sharing and modification.** Snowdrift.coop would not exist without the numerous past works upon which we are built. When modification requires explicit permission, changes quickly become prohibitively difficult. Popular projects may not have time to review and approve every request. And after just a few iterations of editing, it becomes completely impractical to get permission from all past contributors. We can achieve broad creative collaboration only by automatically granting everyone the rights to access, share, modify, and redistribute.

People must also be able to avoid artificial obsolescence and lock-in. We need the freedom to investigate and repair the technology upon which we rely.^[See [iFixit.org](http://ifixit.org/) for *excellent* overview and explanations about the issues with freedom to repair things and the problems with proprietary restrictions and planned obsolescence. iFixit.org is connected with iFixit.com, a mostly-wonderful business enabling people to repair their hardware. Unfortunately, the iFixit.org content has no clear license and the iFixit.com material has a non-free NC license. Hypocritically, they also produce proprietary SaaS software for the development of their manuals, which creates the sort of monopoly and restrictive control for their software that they despise in other products (although at least they made the connected oManual file format fully FLO). Perhaps if Snowdrift.coop succeeds, we might get them to drop the proprietary elements of their business and join us…]

![](/assets/nina/ME_382_LockedUpTechnology2.gif)

Incidentally, some technologists do not understand other fields and make careless assumptions about freedoms being fundamentally different for software and other technology versus for culture and personal expressions. The Free Software Foundation unfortunately licenses their political writing with a no-derivatives (ND) clause, restricting our ability to adapt their work here! This issue (as well as the non-commercial issue mentioned below) are thoroughly addressed in [Nina Paley's *Rantifesto*](http://blog.ninapaley.com/2011/07/04/rantifesto/) about Free Culture.

## A reliable, consistent platform {#consistent}

As anyone who has tried to navigate the process of obtaining permissions from a traditional copyright holder can attest, there is great value in knowing license terms up front. Even when licenses are listed, a platform that includes both FLO and proprietary works puts the burden on users to check and to understand the particular status of each item.

At Snowdrift.coop, patrons do not have to read the fine print or recognize the exact names of various licenses to see whether they are FLO.^[Of course, complete license information will always be available for those who have reasons to care about the specifics.] ***All* projects here are guaranteed to be FLO and thus free of problematic restrictions.**

## Understanding FLO licenses

Note: **We defer to the definitions and license determinations provided by
respected authorities for each class of project, as described in our [project requirements](/project-reqs)**.
We are not creating our own definition for FLO and do not directly approve or disapprove of specific licenses.

As some people new to these concepts may have misunderstandings, here are some important clarifications that apply to FLO licenses generally:

### FLO licenses respect authorship {#plagiarism}

**FLO licensing has nothing to do with plagiarism.** Allowing adaptation and sharing does *not* mean allowing false claims of authorship or affiliation.

The standard Creative Commons licenses all require that the original author receives credit, that modified versions of works get clearly marked, and that any statements of endorsement are removed. Authors can choose to endorse particular publishers, can continue to publish original versions of their works, and can express approval or disapproval of new uses and modifications (for instance, authors can permit approved derivative works to display the [Creator Endorsed Mark](http://questioncopyright.org/creator_endorsed_mark)).

FLO does not mean giving up trademark rights either. Many FLO works are trademarked.^[The permissiveness in how projects treat trademark use varies greatly. Some FLO projects are highly protective of trademark while others oppose strong trademark protection.] Also, authors always have legal protections from slander and libel and the right to charge plagiarizers for misattribution and fraud.

### FLO licenses are not anti-commerce {#nc}

**Licenses with non-commercial (NC) clauses are *not* FLO.** None of the
authorities we defer to in our [project requirements](/project-reqs) consider such clauses acceptable. Below, we offer some explanation about the relevant issues with such restrictions.

First, usage restrictions — including non-commercial clauses — lead to *permission culture* and stand in opposition to free, natural creativity. Commerce should be *encouraged*; constructive economic activity is a social good. Any definition of "commercial use" is extremely fuzzy anyway, so even projects with very limited or debatable commercial connections often avoid NC resources out of fear that their use will be construed as a violation of the non-commercial clause.

Some people use NC licenses to reserve exclusive commercial rights. In fact, a better name for NC would be CRR: Commercial Rights Reserved. By requiring special permission for any commercial use, projects hope to increase their royalties or other compensation. However, in most cases, the NC clause simply results in the work being used less. Even in rare cases where it actually helps with monetization, this use of NC restrictions is based on the pay-for-access proprietary funding model that Snowdrift.coop exists to replace.

Others choose NC licenses to block use of their works by big corporations or other interests that they don't like. They may oppose particular companies or have general concerns about capitalist exploitation. However, copyleft licenses (such as CC-BY-SA) address this concern in most cases without harming positive uses by smaller businesses. Because typical hated corporations are usually unwilling to release *their* derivative works with a FLO license, they will not use copyleft resources. Copyleft licenses respect the freedoms of everyone who accepts their terms for derivative works — including ethical commercial entities.

**Perhaps the major flaw in NC licenses is their incompatibility with truly FLO licenses.** Thus, NC hurts the *non-commercial* users that the license is meant to protect! For example, Wikimedia works are copyleft (CC-BY-SA); and that means you can use them for any purpose — commercial or otherwise — so long as you give credit and license your derivatives the same way. Because NC licenses impose incompatible restrictions, NC-licensed material cannot be mixed with any CC-BY-SA material. A work cannot legally both prohibit and permit commercial use. So, NC-licensed material cannot be used on Wikipedia, and any project that draws resources from Wikipedia or any other CC-BY-SA source cannot also use NC-licensed material — even if absolutely no uses ever have any connection to commercial activities!

For further explanations of the problems with NC licenses, see [Freedom Defined](http://freedomdefined.org/Licenses/NC). For a concrete example of NC compatibility problems see the [Brain Parts Song video](http://blog.wolftune.com/2011/07/brain-parts-song-video.html) by Snowdrift.coop's own co-founder, Aaron Wolf.

#### Note on copyfarleft / Peer Production licenses

Some licenses do not ban commercial use entirely but discriminate in terms of who can have that right. For example, *copyfarleft* licenses block commercial use *by capitalists* specifically (allowing it for non-profits and co-ops etc). While that direction is more aligned with our values in some ways, it has all the same compatibility problems as other commercial-restrictive licenses.

## Why people support FLO works

Coming from the pay-for-access assumptions of copyright law, it may not be clear why anyone would fund projects when they have access anyway.

![](/assets/nina/ME_427_MoralImperative-640x199.png)

What motivates supporters of FLO projects? Some people care about their own freedoms or about the broader ethical problems with proprietary works. Others are more pragmatic: A fan may support ongoing work by their favorite artist. A school may fund authors of educational resources. Businesses may fund ongoing development of software they use.

Some people may contribute by starting new projects. Some volunteer to improve existing works, provide feedback, or give other indirect support. Others may be patrons who provide financial support.

FLO projects already have community support, Snowdrift.coop just makes it easier, more effective, and more inviting for new people to join in.
