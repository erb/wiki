---
title: Snowdrift.coop's Vision for Democracy 
categories: communications
...

The word "*democracy*" has many interpretations. For us, democracy means all citizens can have a real say in decision-making.

Of course, achieving effective and productive democracy requires good mechanisms. For an inferior and often dysfunctional model, consider the majority-rule voting process in today's "democratic" representative governments.

![](/assets/nina/ME_509_Bipartisan-640x199.png)

Corruption, partisan bickering, minority disenfranchisement, and other problems lead many people to reject politics altogether. Yet apathy and inaction mean implicit support the status quo.

![](/assets/nina/ME_504_Political-640x199.png)

We need a better approach that empowers all stakeholders and emphasizes *consensus*.


## Consensus and democracy in our mechanism

The Snowdrift.coop [pledge mechanism](mechanism) has a democratic nature. Projects can only get substantial support when many people together pledge. Each pledge is something like a vote for the community to put our resources toward a certain project.

Those who work on many projects will end up focusing on the ones that the community supports most. The interplay of project-team decisions and patron pledges and feedback provides a natural process that helps address everyone's concerns.

In today's market otherwise, we make strategic compromises. Rather than throw away funds donating to a failing project, I may join others in supporting a less ideal option (similar to the [Snowdrift Dilemma](snowdrift-dilemma)!) With Snowdrift.coop, there is finally a risk-free way to register your support for the ideal. You can pledge to all projects you like, and your support will ultimately go toward whichever projects the rest of the community agrees on. Perhaps your ideal project will have a real chance to succeed after all.

## FLO values and democracy

Free/libre/open projects inherently allow democratic influence because anyone with enough capacity and interest can use and adapt the resources as they wish. The empowered citizens in this free market of ideas make the decisions about which particular versions of FLO resources get adopted and supported. Proprietary control is inherently undemocratic.

## Cooperative governance

Distributed and independent decisions make sense in many cases, but sometimes we need coordinated democratic governance. The [co-op](co-op) structure of Snowdrift.coop uses the best approaches that we know of for running an effective democracy where minority opinions are respected and all stakeholders are fairly represented.

### Score voting

Our co-op elections will use [score voting](http://rangevoting.org) (also known as *range* voting). Instead of awkward forced-ranking (with its endless debates about different calculations) or limited single-choice options, voters simply give each candidate a score on a range of, say, 1-10. You can give several candidates the same score if you like. You can abstain from scoring a candidate when unsure. The candidate with the highest average score wins.

With score voting, you can express your judgments honestly because supporting your ideal candidate doesn't mean losing the chance to support a more likely second-choice option. Score voting offers a natural and comfortable process that favors those outcomes that everyone agrees on most instead of polarizing outcomes favored by only a plurality of voters.

### Facilitated consensus for Board decisions

The elected Board of Directors will make decisions by facilitated consensus with a range-voting fallback, as described in our Bylaws (see [Legal](/legal)). Consensus assures that minority concerns are included, and the fallback addresses the problems with strict-consensus approaches.

## Inspiring a democratic economy

Unregulated free markets deal poorly with public concerns like long-term sustainability, protection of the commons, and issues of externalities. On the other hand, top-down regulations can have unintended side-effects, introduce costly bureaucracy, and be susceptible to regulatory capture. No simple solutions exist aside from continual effort to maintain the best balance.

We hope Snowdrift.coop will become a model for how to achieve effective economic democracy with minimal bureaucracy but essential systems that empower all stakeholders.

### More participants

Most fundraising focuses on reaching out to the wealthiest donors, but that emphasizes the people who already have exceptional power.

![](/assets/nina/ME_348_Merit-640x199.png)

Instead, when projects get their funding from average citizens, they will tend to focus more on the interests of the general public. Wikimedia made a conscious decision to focus their fundraising only on many small donations, and that strategy has succeeded both financially and in keeping their community focus. Of course, Wikimedia is exceptionally popular, and only a small minority of their audience donates. Snowdrift.coop aims to help all sorts of projects to get larger quantities of regular folks providing economic support.

We aim beyond those people who currently donate to projects. We want to provide an alternative to the proprietary market where people pay license fees between $1 and $100 for apps, ebooks, and song downloads. Successful apps with many thousands of customers do good business. Of course, proprietary funding isn't limited to one-time purchases. Millions of people pay subscriptions to access proprietary resources and pay for new upgrades and editions. We don't want all those proprietary works to disappear (although *many* are redundant clutter and *should* go away) — we just want to encourage them to become FLO and be more democratic and accountable to the community.

Thousands of people making very small pledges can adequately support many projects. In a world with great wealth inequity, everyone deserves the chance to be a patron, proudly doing their part. And getting more people involved helps to build the community, whether or not it maximizes the funding.

## Freedom isn't free

Not only do we need to fund essential work, we must all stay active and engaged for democracy to function. At Snowdrift.coop, we work to build a participatory culture and to provide useful tools to keep the community connected with projects and to encourage volunteers.
