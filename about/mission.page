---
title: Mission
categories: governance
...

## Our mission

**Snowdrift.coop facilitates community support for projects that develop public goods.** Our online platform coordinates patrons in providing long-term funding for art, education, science, technology, and other sorts of non-rivalrous works under free/libre/open terms where the rights to access, use, modify, and distribute are *not* exclusively reserved. As a cooperative, we operate democratically so that our policies address the concerns of all stakeholders and serve the general public interest.

## Our vision

We envision a world where everyone has access to a robust and vibrant public commons; where everyone is empowered to realize and share their contributions to our cultural heritage and to participate in the ongoing development of science and technology; and where there is liberty, privacy, and human dignity for all.

## Our aims

### Increase the predominance of high-quality FLO resources

Robust movements today promote FLO works, but they struggle against a status quo which gives power and financial support to those who lock up their projects with legal and technical restrictions.

We aim to reduce reliance on proprietary works and reduce fragmentation in the FLO world by helping to build and sustain communities of supporters who volunteer funds and time to the most promising FLO projects.

### Allow natural market pressures while maintaining higher values

The Snowdrift.coop system provides an effective marketplace where bottom-up pressures promote the best projects; yet we maintain higher values than those concerned only with the market itself.

### Provide constructive work through FLO development

Non-rival FLO works encourage equity because anyone can access them freely — regardless of socio-economic status. Still, such resources require time and energy to create, to improve, and even to promote. Through our financial support for FLO development, we aim to increase the number of people in the world who make a sustainable living doing meaningful, constructive work.

### Be a model for cooperative, honorable, FLO platforms

Online "platforms" facilitate connections and collaboration between users and publish user-generated works. The community of users themselves create the *primary* value on such platforms, yet many operate as "walled gardens" where the platform owners "capture" and monetize the value that the community generates.

We believe that platforms must better respect the values of democracy and liberty. As a community-owned co-op, we aim to be an ethical role model, to maximize our own use and development of FLO tools, and to help others follow our example.
