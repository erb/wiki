---
title: Join the Snowdrift.coop Team 
...

Welcome! We're glad you've taken an interest in helping us.

People involved with Snowdrift.coop in a recognized formal sense *prior* to our full incorporation and launch fill two types of roles: (A) primary team and (B) steering committee / advisors. Many other informal volunteers and general community members help in lots of ways but haven't accepted particular responsibilities or titles.

For reference, our [how-to-help](how-to-help) page broadly describes the various ways to participate.

### Current team members

Our [Who We Are page](/p/snowdrift/who) lists many of the people involved; but as of this writing (2016-02-22), that list needs updating. We plan to update these listings, indicate past vs current participants, and better clarify titles and distinctions between main team and advisors. Two other places we are currently organizing listings (both work-in-progress, not finalized) are: the [teams wiki page](/governance/teams) and our groups organizing at [shovel.snowdrift.coop](http://shovel.snowdrift.coop/projects/snowdrift) which runs the Open Project software.

## Responsibilities of advisors

The responsibilities of each advisor include:

* Read and understand the core writings, including the links from the [about](about) and [next-steps](next) pages
* Keep up with overall progress in order to offer informed guidance
* Respond to e-mails or other specifically committee-directed communications
* Keep Snowdrift.coop in mind, updating us with relevant references that you encounter and mentioning and advocating for us in relevant contexts as appropriate

## Team Responsibilities

The responsibilities of each team member include:

* Handle the defined scope of authority and responsibilities for your particular role(s) (which may include designating responsibilities to other roles)
* Be available within a reasonable time-frame when others need your involvement and at least communicate clearly when availability may be limited for some reason.
* Assure that some representative from each main internal team attends our weekly check-in meetings or at least sends regrets and provide an email update on team progress.
* If you cannot maintain your role(s) going forward, communicate the situation clearly and make an effort to find someone else to fill the role(s).

## Qualities desired in participants

Everyone involved should generally support the values of FLO Software and Culture. Otherwise, we thrive with diverse opinions and perspectives; we would like to include as many different backgrounds and perspectives as possible. As we intend to have international scope, we welcome international representation on the committee.

## Apply to join

**To express interest (or at least willingness) in joining us, please fill out our [volunteer form](/p/snowdrift/volunteer)**.

If you know others who you believe may be interested, invite them to view this page and the rest of our proposals.

## No compensation at this time

At this point, we rely on volunteered time. Participation offers great networking and learning experience and the chance to see Snowdrift.coop succeed. Once launched, we hope to better raise funds for ongoing development as well as to fund whatever other projects you want to work on. Of course, if we are as successful as we hope, then having had a formal position with Snowdrift.coop will also make a valuable resumé item.
