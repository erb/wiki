---
title: Publicizing Snowdrift.coop
categories: communications
...

This page discusses outside channels by which we promote Snowdrift.coop.

After launching, projects themselves will be a large part of reaching out to their audiences.

Places we have engaged and suggest at least considering for future publicity:

## News aggregates

* [Reddit](https://reddit.com) (which is an imperfect ad-driven site that at least runs on free software and respects privacy)
    * especially these subreddits:
        * [Haskell subreddit](http://www.reddit.com/r/haskell/)
        * [Free Software subreddit](http://www.reddit.com/r/freesoftware/)
        * [Free Culture subreddit](http://www.reddit.com/r/freeculture/)
        * [Cooperatives subreddit](http://www.reddit.com/r/cooperatives/)
        * [Crowdfunding subreddit](http://www.reddit.com/r/Crowdfunding/) (less so)
        * [Open Source subreddit](http://www.reddit.com/r/opensource/)
        * There are several subreddits that aren't popular but are appropriate and may be others worth considering that we haven't listed here…
        * The [Snowdriftcoop subreddit](https://www.reddit.com/r/Snowdriftcoop) is a holding place, not used (as of August 2015)
    * [user/Snowdrift-coop](https://www.reddit.com/user/Snowdrift-coop/) is an official user representing us but is not currently in use.^[currently, [Aaron](/u/3) is the only one with access, but we'll adjust that later as we get things more organized. We may prefer to just have individuals always represent themselves, not sure.]
* [Hacker News](http://news.ycombinator.com/)
* [Slashdot](https://slashdot.org)
* [FreePost](http://freepo.st) — a FLO dedicated news/discussion/group system
* [WeCo](https://www.weco.io/) — a FLO & co-op Reddit alternative

## Social networks

* **Mastodon**: [@snowdrift@social.coop](https://social.coop/@snowdrift)
* Twitter: @SnowdriftCoop
* Others: Obviously, individuals may have accounts on all sorts of services including Google+, Facebook, Pinterest, Tumblr, Instagram, etc, etc), but we don't have any formal connection to those at this time.

## Mailing lists and fora

* [Libreplanet discuss](https://lists.libreplanet.org/mailman/listinfo/libreplanet-discuss)
* [FSF Community Team list](https://lists.gnu.org/mailman/listinfo/fsf-community-team)
* [Open Knowledge discussion forum](http://discuss.okfn.org/) and [Open Knowledge discuss list](https://lists.okfn.org/mailman/listinfo/okfn-discuss) — generally for formal Open Knowledge projects, but sympathetic community, not overly formal
* [FLOSS Foundations](http://lists.freedesktop.org/mailman/listinfo/foundations) — a semi-private list for non-profit foundations related to FLO software
* The affiliate list for OSI (private list for official OSI affiliates), affiliates@lists.opensource.org
* [Platform Cooperativism list](https://lists.riseup.net/www/info/platformcoop-discuss)
* [Community Leadership Forum](http://communityleadershipforum.com/) — not FLO-dedicated but large FLO presence, general community building

## IRC channels

All at freenode.net: #haskell, #haskell-beginners, #fsf, #gnu, #opensourcemusicians, #libreplanet, #openhatch, #freeculture, #opensourcedesign

(What about Matrix rooms etc?)

## Press

See our [press](/press) page for particular articles written about Snowdrift.coop and general press info.

## Conferences

Some of us have attended several conferences, and we should try to connect with more. In some cases, conferences have been convenient enough for someone from our team to attend, but we hope other volunteer liaisons can help us promote and network at other events. Presenting will be ideal, but attending otherwise is also often worthwhile.

* [**LibrePlanet**](https://libreplanet.org) — among the most strongly aligned events, the Free Software Foundation's annual conference in Cambridge, MA
* [Open Source Bridge](http://opensourcebridge.org) — excellent community-based tech conference in Portland, OR with a social justice / diversity ethic (UPDATE: defunct after 2018)
* [Platform Cooperativism](https://platform.coop/events)
* [Community Leadership Summit](http://www.communityleadershipsummit.com) — unconference hosted just before OSCON in Portland, OR
* [OSCON](https://www.oscon.com) — corporate enormous event by O'Reilly
* [Indie Web Summit](https://indieweb.org/)
* [SeaGL](httpt://seagl.org) — Seattle GNU/Linux conference
* [FOSDEM](https://fosdem.org) — European enormous conference
* [Linux Fest Northwest](http://linuxfestnorthwest.org)
* [Allied Media Conference](http://alliedmedia.org)
* [Write the Docs](http://conf.writethedocs.org)
* [BayHac](http://bayhac.org/) — Haskell hacking weekend
* [SCaLE](http://www.socallinuxexpo.org) — significant Linux event in Los Angeles
* *more* (this is just a short starter list)

(There are tons of other smaller or random tech conferences, and we should organize them in some sort of priority order with more information. Maybe we could also list general notes about local meetups that are the sorts of places to promote: GNU/Linux groups, Wikipedia meetups, Hacker Spaces, etc. etc. within reason…)

## Other

* [Haskell Communities report](https://wiki.haskell.org/Haskell_Communities_and_Activities_Report)
